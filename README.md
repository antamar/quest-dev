# Öffentliches Repositorium für Antamarquesten

Dieses Projekt ist für größere Questen gedacht und gut geeignet, wenn mehrere Leute in irgendeiner Form an der Quest arbeiten wollen, z.B. durch mit einem integrativem Review-Prozess.

Aber auch für das eignee Management kann GitLab sehr gut geeignet sein, weil man sich Aufgaben definieren kann (Issues).

# Mitmachen

Mitmachen kann hier eigentlich jeder in einer geeigneten Form, allerdings sollte man sich ein bisschen in Git einlesen. Vieles darin ist kompliziert, aber ein einfaches Fork-Pull-Push-Merge-Request ist recht einfach.


- Issues sollen immer mit einem Label für eine Quest versehen werden, damit man gleich sieht, wozu er gehört.
- Um dem Projekt hinzugefügt zu werden, um z.B. eine neue Quest reinzutun: Tommek kontaktieren!
- Bei Fragen: Tommek kontaktieren!
